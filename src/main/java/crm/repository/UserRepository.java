package crm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import crm.config.MysqlConnection;
import crm.models.User;

public class UserRepository {
	 public List<User> getUsernameAndPassword(String username,String password) {
		 List<User> list = new ArrayList<>();
		 Connection connection = MysqlConnection.getConnection();
		 String query = "SELECT * FROM users u WHERE u.username=? and u.password=?";
		 try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, username);
			preparedStatement.setString(2, password);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setFullname(resultSet.getString("fullname"));
				user.setAge(resultSet.getInt("age"));
				user.setUsername(resultSet.getString("username"));
				
				list.add(user);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 finally {
			try {
				if(connection!=null) {					
					connection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		 return list;
	 }
}
