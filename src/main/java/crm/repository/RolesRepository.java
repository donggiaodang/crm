package crm.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import crm.config.MysqlConnection;
import crm.models.Roles;
import crm.models.User;

public class RolesRepository {
	 public List<Roles> getRoles() {
		 List<Roles> list = new ArrayList<>();
		 Connection connection = MysqlConnection.getConnection();
		 String query = "SELECT * FROM roles";
		 try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			
			
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				Roles role = new Roles();
				role.setId(resultSet.getInt("id"));
				role.setRoleName(resultSet.getString("role_name"));
				role.setDescription(resultSet.getString("description"));
			
				
				list.add(role);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 finally {
			try {
				if(connection!=null) {					
					connection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		 return list;
	 }
}
