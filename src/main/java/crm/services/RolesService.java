package crm.services;

import java.util.List;

import crm.models.Roles;
import crm.repository.RolesRepository;

public class RolesService {
private RolesRepository repository = new RolesRepository();
public List<Roles> getRoles(){
	return repository.getRoles();
}
}
