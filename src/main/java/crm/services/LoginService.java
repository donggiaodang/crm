package crm.services;

import java.util.List;

import crm.models.User;
import crm.repository.UserRepository;

public class LoginService {
	private UserRepository userRepository = new UserRepository();

	public boolean checkLogin(String username, String password) {
		List<User> list = userRepository.getUsernameAndPassword(username, password);
		return list.size() > 0 ? true : false;
		
	}
}
