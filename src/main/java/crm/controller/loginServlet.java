package crm.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import crm.services.LoginService;

@WebServlet(name = "loginServlet", urlPatterns = { "/login" })
public class loginServlet extends HttpServlet {
	private LoginService loginService = new LoginService();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub

		req.getRequestDispatcher("/login.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String username = req.getParameter("email");
		String password = req.getParameter("password");
		boolean isLogin = loginService.checkLogin(username, password);
		System.out.println("isLogin: "+isLogin);
//		if (username.equals("admin") && password.equals("123456")) {
//			resp.sendRedirect(req.getContextPath() + "/hello");
//		} else {
//			resp.sendRedirect(req.getContextPath() + "/login");
//		}
	}
}
